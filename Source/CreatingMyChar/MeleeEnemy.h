// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseEnemy.h"
#include "MeleeEnemy.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API AMeleeEnemy : public ABaseEnemy{
	GENERATED_BODY()
public:
	AMeleeEnemy();

	void Attack() override;

	void MakeDamage(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

protected:
	UPROPERTY(VisibleAnywhere, Category = "Attack Capsule")
		class UCapsuleComponent* AttackCapsule;

	UPROPERTY(EditDefaultsOnly, Category = "Attack Capsule")
		FName AttackCapsuleSocketName = "head";
};
