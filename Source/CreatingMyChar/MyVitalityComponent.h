// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MyVitalityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CREATINGMYCHAR_API UMyVitalityComponent : public UActorComponent{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMyVitalityComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// vitality regeneration logic
	UFUNCTION()
		void HealthRegeneration();

public:	
	// starts vitality regeneration
	UFUNCTION()
		void HealthRegenerationStart();

	// stops vitality regeneration
	UFUNCTION()
		void HealthRegenerationStop();

	// returns current vitality points
	UFUNCTION()
		float GetCurrentHealthPoint() const;

	// sets current vitality points
	UFUNCTION()
		void SetCurrentHealthPoint(const float &HP);

	// returns maximal vitality points
	UFUNCTION()
		float GetMaxHealthPoint() const;

	// checks if current vitality points more zero
	UFUNCTION()
		bool IsAlive() const;
		
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// dealing damage logic
	UFUNCTION()
		void DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

protected:
	// maximum vitality points value
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
		float MaxHealthPoint = 100;

	// current vitality points value
	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float CurrentHealthPoint;

	// vitality regeneration points size
	UPROPERTY(EditDefaultsOnly, Category = "Health")
		float RegenerationHealthPointSize;

	// vitality regeneration speed multiplyer
	UPROPERTY(EditDefaultsOnly, Category = "Health")
		float RegenerationTimerSpeed;

	// delay before vitality regeneration starts
	UPROPERTY(EditDefaultsOnly, Category = "Health")
		float RegenerationTimerDelay;

	// timer to start vitality regeneration
	UPROPERTY()
		FTimerHandle RegenetationTimer;

	// delay to start vitality regeneration after taking damage
	UPROPERTY(EditDefaultsOnly, Category = "Health")
		float AfterDamageRegenerationTimerDelay;

	// score points 
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Score")
		int32 ScorePoints;

public:
	// on vitality points changed signal emiter
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegate OnHealthChanged;

	// on owner died signal emiter
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegate OnOwnerDied;
}; 
