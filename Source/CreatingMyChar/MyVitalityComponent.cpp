// Fill out your copyright notice in the Description page of Project Settings.


#include "MyVitalityComponent.h"
#include "TimerManager.h"
#include "MyGameInstance.h"

// Sets default values for this component's properties
UMyVitalityComponent::UMyVitalityComponent(){
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	CurrentHealthPoint = MaxHealthPoint;
}

// Called when the game starts
void UMyVitalityComponent::BeginPlay(){
	Super::BeginPlay();
	if (GetOwner()) {
		GetOwner()->OnTakeAnyDamage.AddDynamic(this,&UMyVitalityComponent::DamageHandle);
	}
	CurrentHealthPoint = MaxHealthPoint;
	OnHealthChanged.Broadcast();
}

void UMyVitalityComponent::HealthRegeneration(){
	if (CurrentHealthPoint + RegenerationHealthPointSize <= MaxHealthPoint){
		CurrentHealthPoint += RegenerationHealthPointSize;
	}
	else{
		HealthRegenerationStop();
	}
	OnHealthChanged.Broadcast();
}

void UMyVitalityComponent::HealthRegenerationStart(){
	if (CurrentHealthPoint < MaxHealthPoint){
		GetWorld()->GetTimerManager().SetTimer(RegenetationTimer, this, &UMyVitalityComponent::HealthRegeneration, RegenerationTimerSpeed, true, RegenerationTimerDelay);
		RegenerationTimerDelay = 0;
	}
	else{
		HealthRegenerationStop();
	}
}

void UMyVitalityComponent::HealthRegenerationStop(){
	GetWorld()->GetTimerManager().ClearTimer(RegenetationTimer);
}

// Called every frame
void UMyVitalityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction){
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UMyVitalityComponent::DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser){
	if (!IsAlive()) {
		UE_LOG(LogTemp, Warning, TEXT("olready dead"));
		return;
	}

	if (IsAlive()) {
		UE_LOG(LogTemp, Warning, TEXT("%s was damaged for %s"), *DamagedActor->GetName(), *FString::SanitizeFloat(Damage));

		CurrentHealthPoint -= Damage;
		OnHealthChanged.Broadcast();

		RegenerationTimerDelay = AfterDamageRegenerationTimerDelay;
		HealthRegenerationStart();
	}
	
	if (!IsAlive()) {
		UMyGameInstance* GameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
		if (GameInstance) {
			GameInstance->AddScore(ScorePoints);
		}
		UE_LOG(LogTemp, Warning, TEXT("Owner died"));
		HealthRegenerationStop();
		OnOwnerDied.Broadcast();
	}
}

float UMyVitalityComponent::GetCurrentHealthPoint() const {
	return CurrentHealthPoint;
}

float UMyVitalityComponent::GetMaxHealthPoint() const {
	return MaxHealthPoint;
}

bool UMyVitalityComponent::IsAlive() const {
	return CurrentHealthPoint > 0;
}

void UMyVitalityComponent::SetCurrentHealthPoint(const float &HP) {
	if (CurrentHealthPoint + HP <= MaxHealthPoint) {
		CurrentHealthPoint += HP;
	}
	else {
		CurrentHealthPoint = MaxHealthPoint;
	}
	OnHealthChanged.Broadcast();
}


