// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseWeapon.h"
#include "WeaponTrace.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API AWeaponTrace : public ABaseWeapon{
	GENERATED_BODY()

public:
	// weapon fire logic
	UFUNCTION()
		void Fire() override;

	// stops weapon fire
	UFUNCTION()
		void StopFire() override;

	// calls every frame
	virtual void Tick(float DeltaSeconds) override;

	// calls when game started
	virtual void BeginPlay() override;

protected:
	// length of trace
	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float TraceLength;

	// debug sphere radius
	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float SphereRadius;

	// debug sphere life time
	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float DebugSphereLifeTime = 1;

	// beam particle effect
	UPROPERTY(EditDefaultsOnly, Category = "Beam")
		UParticleSystem* Beam;

	// beam particle effect component
	UPROPERTY(BlueprintReadOnly, Category = "Beam")
		class UParticleSystemComponent* BeamComponent;

	// beam particle effect scale factor
	UPROPERTY(EditDefaultsOnly, Category = "Beam")
		FVector BeamScale;
};
