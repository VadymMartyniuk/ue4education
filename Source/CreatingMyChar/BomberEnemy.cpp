// Fill out your copyright notice in the Description page of Project Settings.


#include "BomberEnemy.h"
#include "BombPickup.h"
#include "MyCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "MyVitalityComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"

ABomberEnemy::ABomberEnemy() {
	GetCapsuleComponent()->SetGenerateOverlapEvents(true);
	//GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ABomberEnemy::MakeDamage);  // does not work - dublicate in Blueprint
}

void ABomberEnemy::Attack(){
	Super::Attack();
	
	if (ExplosionParticle) {
		UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionParticle, GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No explosion particle"));
	}
}

void ABomberEnemy::MakeDamage(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){
	PlayerCharacter = Cast<AMyCharacter>(OtherActor);
	if (PlayerCharacter) {
		bIsSelfDestroy = true;
		Attack();
		Super::MakeDamage(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
		VitalityComponent->SetCurrentHealthPoint(0);
		VitalityComponent->OnOwnerDied.Broadcast();
		Destroy();
	}
}
