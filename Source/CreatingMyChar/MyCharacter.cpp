// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacter.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "ConstructorHelpers.h"
#include "RotationMatrix.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BaseWeapon.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Components/ArrowComponent.h"
#include "MyVitalityComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"


// Sets default values
AMyCharacter::AMyCharacter() {
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//created default springArm object and attached it to rootObject
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);

	//created default camera object and attached it to springArm object and set length
	PlayerCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	PlayerCameraComponent->SetupAttachment(SpringArmComponent);
	SpringArmComponent->TargetArmLength = DefaultCameraLength;

	//adds skeletal mesh object to character object
	//check(SkelMesh != nullptr);
	if (SkelMesh) {
		GetMesh()->SetSkeletalMesh(SkelMesh);
		//rotates skeletal mesh in colliding capsule
		GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));
	}

	bUseControllerRotationYaw = false;
	SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritRoll = false;
	SpringArmComponent->bInheritYaw = false;

	GetCharacterMovement()->MaxWalkSpeed = DefaultSpeed;

	RightFoot = CreateDefaultSubobject<UArrowComponent>(TEXT("RighFoot"));
	LeftFoot = CreateDefaultSubobject<UArrowComponent>(TEXT("LeftFoot"));
	RightFoot->SetupAttachment(GetMesh(), RightFootSocketName);
	LeftFoot->SetupAttachment(GetMesh(), LeftFootSocketName);

	VitalityComponent = CreateDefaultSubobject<UMyVitalityComponent>(TEXT("VitalityComponent"));
	VitalityComponent->OnOwnerDied.AddDynamic(this,&AMyCharacter::CharacterDied);
}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay(){
	Super::BeginPlay();
	CurrentWeapon = SpawnWeapon();
	AttachWeapon(WeaponBackSocket);
	CurrentWeaponSocket = WeaponBackSocket;
}

UMyVitalityComponent* AMyCharacter::GetVitalityComponent() {
	return VitalityComponent;
}

void AMyCharacter::CharacterDied(){
	StopFire();

	GetMesh()->SetEnableGravity(true);
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	FVector ImpulseVector = GetActorForwardVector() * -1 * DiedImpulse;
	GetMesh()->AddImpulse(ImpulseVector, "head", true);

	GetCharacterMovement()->DisableMovement();
	
	CurrentWeapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	CurrentWeapon->GetStaticMeshComponent()->SetEnableGravity(true);
	CurrentWeapon->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CurrentWeapon->GetStaticMeshComponent()->SetSimulatePhysics(true);
	CurrentWeapon = nullptr;
}

ABaseWeapon* AMyCharacter::SpawnWeapon() {
	if (WeaponToSpawnClass) {
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SPawnParams;
		SPawnParams.Owner = this;
		UE_LOG(LogTemp, Warning, TEXT("Spawn weapon"));
		return Cast<ABaseWeapon>(GetWorld()->SpawnActor(WeaponToSpawnClass, &TmpTransform, SPawnParams));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Spawn weapon failed"));
		return nullptr;
	}
}

void AMyCharacter::SetCurrentWeapon() {
	CurrentWeapon = SpawnWeapon();
	if (CurrentWeaponSocket == WeaponArmSocket) {
		AttachWeapon(WeaponArmSocket);
		CurrentWeaponSocket = WeaponArmSocket;
	}
	else if (CurrentWeaponSocket == WeaponBackSocket) {
		AttachWeapon(WeaponBackSocket);
		CurrentWeaponSocket = WeaponBackSocket;
	}
}

void AMyCharacter::HideShowWeaponBack() {
	if (CurrentWeapon) {
		if (!GetMesh()->GetAnimInstance()->Montage_IsPlaying(CurrentWeapon->EquipMontage)) {
			CurrentWeapon->PlayEquipMontage();
			FTimerHandle EquipTimer;
			GetWorld()->GetTimerManager().SetTimer(EquipTimer, this, &AMyCharacter::EquipWeapon, CurrentWeapon->TimeToAttachWeapon, false, 1);
		}
	}
}

void AMyCharacter::EquipWeapon() {
	if (CurrentWeapon) {
		if (CurrentWeaponSocket == WeaponArmSocket) {
			AttachWeapon(WeaponBackSocket);
			CurrentWeaponSocket = WeaponBackSocket;
			if (UnEquipSound) {
				UGameplayStatics::PlaySoundAtLocation(this, UnEquipSound, GetActorLocation());
			}
			OnWeaponUnEquip.Broadcast();
		}
		else if (CurrentWeaponSocket == WeaponBackSocket) {
			AttachWeapon(WeaponArmSocket);
			CurrentWeaponSocket = WeaponArmSocket;
			if (EquipSound) {
				UGameplayStatics::PlaySoundAtLocation(this, EquipSound, GetActorLocation());
			}
			OnWeaponEquip.Broadcast();
		}
	}
}

void AMyCharacter::AttachWeapon(FName SocketName) {
	if (CurrentWeapon) {
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
		OnGetCurrentWeapon.Broadcast();
	}
}

void AMyCharacter::DestroyCurrentWeapon() {
	CurrentWeapon->Destroy();
}

ABaseWeapon* AMyCharacter::GetCurrentWeapon() {
	return CurrentWeapon;
}

void AMyCharacter::SetWeaponToSpawnClass(const TSubclassOf<ABaseWeapon> &WeaponClass) {
	WeaponToSpawnClass = WeaponClass;
}

TSubclassOf<ABaseWeapon> AMyCharacter::GetWeaponToSpawnClass() const {
	return WeaponToSpawnClass;
}

FName AMyCharacter::GetCurrentWeaponSocket() const{
	return CurrentWeaponSocket;
}

FName AMyCharacter::GetWeaponBackSocket() const {
	return WeaponBackSocket;
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
	RotateToMouse();
}

//Rotates character to mouse cursor
void AMyCharacter::RotateToMouse(){
	if (VitalityComponent->IsAlive()) {
		FHitResult Hit;
		if (GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit)) {
			float RotationYaw = FRotationMatrix::MakeFromX(Hit.ImpactPoint - GetActorLocation()).Rotator().Yaw;
			SetActorRotation(FRotator(0, RotationYaw, 0));
		}
	}
}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent){
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMyCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AMyCharacter::MoveRight);
	PlayerInputComponent->BindAction(TEXT("Run"),IE_Pressed, this, &AMyCharacter::SprintModeOn);
	PlayerInputComponent->BindAction(TEXT("Run"), IE_Released, this, &AMyCharacter::SetDefaultSpeed);
	PlayerInputComponent->BindAction(TEXT("CameraZoomIn"),IE_Pressed, this, &AMyCharacter::ZoomCameraIn);
	PlayerInputComponent->BindAction(TEXT("CameraZoomOut"), IE_Pressed, this, &AMyCharacter::ZoomCameraOut);
	PlayerInputComponent->BindAction(TEXT("Walk"),IE_Pressed,this,&AMyCharacter::WalkModeOn);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Released, this, &AMyCharacter::WalkModeOff);
	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &AMyCharacter::WeaponReload);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AMyCharacter::StartFire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &AMyCharacter::StopFire);
	PlayerInputComponent->BindAction(TEXT("EquipWeapon"), IE_Pressed, this, &AMyCharacter::HideShowWeaponBack);
	PlayerInputComponent->BindAction(TEXT("FireMode"), IE_Pressed, this, &AMyCharacter::SetFireMode);
}

//Moves character forward and back when input bindings active
void AMyCharacter::MoveForward(float ScaleValue){
	if (GetCharacterMovement()->MaxWalkSpeed <= DefaultSpeed){
		AddMovementInput(FVector(1, 0, 0), ScaleValue);
	}
	else if(GetCharacterMovement()->MaxWalkSpeed > DefaultSpeed && ScaleValue != 0) {
		AddMovementInput(GetActorForwardVector());
	}
}

//Moves character right and left when input mappings active
void AMyCharacter::MoveRight(float ScaleValue){
	if (GetCharacterMovement()->MaxWalkSpeed <= DefaultSpeed) {
		AddMovementInput(FVector(0, 1, 0), ScaleValue);
	}
	else if (GetCharacterMovement()->MaxWalkSpeed > DefaultSpeed && ScaleValue != 0) {
		AddMovementInput(GetActorForwardVector());
	}
}

float AMyCharacter::GetCurrentSpeed() const{
	return GetCharacterMovement()->MaxWalkSpeed;
}

void AMyCharacter::SetBoostedSpeed(){
	GetCharacterMovement()->MaxWalkSpeed *= SpeedBoostMultiplyer;
}

void AMyCharacter::SetDefaultSpeed(){
	GetCharacterMovement()->MaxWalkSpeed = DefaultSpeed;
}

void AMyCharacter::RightStepTrigger(){
	SpawnStepSmoke(RightFoot, RightFootSocketName);
	PlayStepSound();
}

void AMyCharacter::LeftStepTrigger(){
	SpawnStepSmoke(LeftFoot, LeftFootSocketName);
	PlayStepSound();
}

void AMyCharacter::SpawnStepSmoke(UArrowComponent* Foot, FName FootSocket) {
	if (StepSmoke) {
		FHitResult Hit;
		FVector StartTrace = Foot->GetSocketLocation(FootSocket) + FVector(0, 0, 20);
		FVector EndTrace = Foot->GetSocketLocation(FootSocket) - FVector(0, 0, 20);
		GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_Visibility);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), StepSmoke, Hit.Location, FRotator::ZeroRotator, FVector(0.2, 0.2, 0.2));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No player step smoke"));
	}
}

void AMyCharacter::PlayStepSound(){
	if (StepSound) {
		UGameplayStatics::PlaySoundAtLocation(this, StepSound, GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No player step sound"));
	}
}

void AMyCharacter::SetFireMode(){
	if (CurrentWeapon) {
		CurrentWeapon->SetFireMode();
	}
}

//sets character MaxWalkSpeed to Sprint speed when input mappings active
void AMyCharacter::SprintModeOn(){
	StopFire();
	GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
}

//sets character MaxWalkSpeed to walk speed when input action active
void AMyCharacter::WalkModeOn(){
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	if (FireRifleIronsightsMontage) {
		if (CurrentWeapon) {
			CurrentWeapon->SetFireMontage(FireRifleIronsightsMontage);
		}
	}
}

//sets character MaxWalkSpeed to default speed when input action active
void AMyCharacter::WalkModeOff(){
	GetCharacterMovement()->MaxWalkSpeed = DefaultSpeed;
	if (FireRifleHipMontage) {
		if (CurrentWeapon) {
			CurrentWeapon->SetFireMontage(FireRifleHipMontage);
		}
	}
}

void AMyCharacter::WeaponReload(){
	if (CurrentWeapon) {
		if (CurrentWeaponSocket == WeaponArmSocket) {
			CurrentWeapon->Reload();
		}
	}
}

void AMyCharacter::StartFire(){
	if (CurrentWeapon) {
		if (CurrentWeaponSocket == WeaponArmSocket) {
			if (GetCharacterMovement()->MaxWalkSpeed <= DefaultSpeed) {
				CurrentWeapon->StartFire();
			}
			else if (GetCharacterMovement()->MaxWalkSpeed > DefaultSpeed && GetCharacterMovement()->Velocity.Size() == 0) {
				CurrentWeapon->StartFire();
			}
		}
	}
}

void AMyCharacter::StopFire(){
	if (CurrentWeapon) {
		CurrentWeapon->StopFire();
	}	
}

//zoom in and zoom out character camera, when input mappings active 
//increase/decrease TargetArmLength in USpringArmComponent by CameraStep value
void AMyCharacter::ZoomCameraIn(){
	if (SpringArmComponent->TargetArmLength - ZoomCameraStep >= MinCameraLength) {
		SpringArmComponent->TargetArmLength -= ZoomCameraStep;
		if (CameraZoomInSound) {
			UGameplayStatics::PlaySound2D(this, CameraZoomInSound);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No camera zoom in sound"));
		}
	}
}

void AMyCharacter::ZoomCameraOut(){
	if (SpringArmComponent->TargetArmLength + ZoomCameraStep <= MaxCameraLength) {
		SpringArmComponent->TargetArmLength += ZoomCameraStep;
		if (CameraZoomOutSound) {
			UGameplayStatics::PlaySound2D(this, CameraZoomOutSound);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No camera zoom out sound"));
		}
	}
}

