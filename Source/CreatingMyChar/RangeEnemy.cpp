// Fill out your copyright notice in the Description page of Project Settings.


#include "RangeEnemy.h"
#include "MyCharacter.h"
#include "BaseProjectile.h"
#include "ImpactEffectComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Private/KismetTraceUtils.h"	

ARangeEnemy::ARangeEnemy() {
	ImpactEffectComponent = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("Impact effect component"));
}

void ARangeEnemy::StopAttack() {
	Super::StopAttack();

	if (BeamComponent) {
		BeamComponent->DeactivateSystem();
	}

	bIsAttacking = false;
}

void ARangeEnemy::EnemyDied() {
	Super::EnemyDied();
	StopAttack();
}

void ARangeEnemy::BeginPlay() {
	Super::BeginPlay();
	if (Beam) {
		BeamComponent = UGameplayStatics::SpawnEmitterAttached(Beam, GetMesh(), MuzzleSocketName, FVector::ZeroVector, FRotator::ZeroRotator, BeamScale, EAttachLocation::SnapToTarget, false);
		StopAttack();
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No beam component"));
	}
}

void ARangeEnemy::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (bIsAttacking) {
		Attack();
	}
}

void ARangeEnemy::Attack() {
	StopAttack();
	
	Super::Attack();

	TArray<FHitResult> HitsArray;
	FVector TraceStart = GetMesh()->GetSocketTransform(MuzzleSocketName).GetLocation();
	FVector TraceEnd = GetMesh()->GetSocketTransform(MuzzleSocketName).GetRotation().GetForwardVector() * TraceLength + TraceStart;

	FCollisionObjectQueryParams CollisionParams;
	CollisionParams.AddObjectTypesToQuery(ECC_PhysicsBody);
	CollisionParams.AddObjectTypesToQuery(ECC_WorldDynamic);
	CollisionParams.AddObjectTypesToQuery(ECC_WorldStatic);

	FCollisionShape CollisionShape = FCollisionShape::MakeSphere(SphereRadius);

	GetWorld()->SweepMultiByObjectType(HitsArray, TraceStart, TraceEnd, FQuat::Identity, CollisionParams, CollisionShape);

	if (BeamComponent) {
		BeamComponent->Activate();
		BeamComponent->SetBeamTargetPoint(0, TraceEnd, 0);
	}

	if (HitsArray.Num() > 0) {
		for (auto TmpHit : HitsArray) {
			if (ImpactEffectComponent) {
				ImpactEffectComponent->SpawnImpactEffect(TmpHit);
			}
			DrawDebugSphere(GetWorld(), TmpHit.ImpactPoint, SphereRadius, 16, FColor::Red, false, 1);
		}
	}

	bIsAttacking = true;
}
