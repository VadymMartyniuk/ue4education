// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponTrace.h"
#include "BaseProjectile.h"
#include "CollisionQueryParams.h"
#include "Private/KismetTraceUtils.h"	
#include "ImpactEffectComponent.h"
#include "ParticleHelper.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

void AWeaponTrace::Fire(){
	if (CurrentMagazineSize > 0) {
		if (bCanFire()) {
			TArray<FHitResult> HitsArray;
			FVector TraceStart = GetMuzzleTransform().GetLocation();
			FVector TraceEnd = GetMuzzleTransform().GetRotation().GetForwardVector() * TraceLength + TraceStart;

			FCollisionObjectQueryParams CollisionParams;
			CollisionParams.AddObjectTypesToQuery(ECC_PhysicsBody);
			CollisionParams.AddObjectTypesToQuery(ECC_WorldDynamic);
			CollisionParams.AddObjectTypesToQuery(ECC_WorldStatic);

			FCollisionShape CollisionShape = FCollisionShape::MakeSphere(SphereRadius);

			GetWorld()->SweepMultiByObjectType(HitsArray, TraceStart, TraceEnd, FQuat::Identity, CollisionParams, CollisionShape);
			
			if (BeamComponent) {
				if (bIsAutoFire) {
					BeamComponent->Activate();
					BeamComponent->SetBeamTargetPoint(0, TraceEnd, 0);
				}
			}
			else{
				DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Purple, false, 1);
			}

			CurrentMagazineSize--;
			OnBulletsCountChanged.Broadcast();

			if (HitsArray.Num() > 0) {
				for (auto TmpHit : HitsArray) {
					if (ImpactEffectComponent) {
						ImpactEffectComponent->SpawnImpactEffect(TmpHit);
					}
					DrawDebugSphere(GetWorld(), TmpHit.ImpactPoint, SphereRadius, 16, FColor::Red, false, 1);
				}
			}
		}
	}
	else {
		Super::Fire();
		StopFire();	
	}
}

void AWeaponTrace::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);
}

void AWeaponTrace::BeginPlay(){
	Super::BeginPlay();
	if (Beam) {
		BeamComponent = UGameplayStatics::SpawnEmitterAttached(Beam, RootComponent, MuzzleSocketName, FVector::ZeroVector, FRotator::ZeroRotator, BeamScale, EAttachLocation::SnapToTarget, false);
		if (BeamComponent) {
			BeamComponent->DeactivateSystem();
		}
	}
}

void AWeaponTrace::StopFire() {
	Super::StopFire();
	if (BeamComponent) {
		BeamComponent->DeactivateSystem();
	}
}


