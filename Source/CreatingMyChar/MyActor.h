// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <time.h>
#include <ctime>
#include <chrono>
#include "Engine/GameEngine.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyActor.generated.h"

UCLASS()
class CREATINGMYCHAR_API AMyActor : public AActor{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void MoveActor(float DeltaTime);
	void CheckPerformance();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	float Step = 200;
	float TimeToFly = 3;
	float CurrTime = 0;
	bool bUpDirection = true;

};
