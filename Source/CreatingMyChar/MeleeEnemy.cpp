// Fill out your copyright notice in the Description page of Project Settings.


#include "MeleeEnemy.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"

AMeleeEnemy::AMeleeEnemy() {
	AttackCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Attack Capsule Component"));
	if (!AttackCapsuleSocketName.IsNone()) {
		AttackCapsule->SetupAttachment(GetMesh(), AttackCapsuleSocketName);
	}
	AttackCapsule->SetGenerateOverlapEvents(false);
	AttackCapsule->OnComponentBeginOverlap.AddDynamic(this, &AMeleeEnemy::MakeDamage);
}

void AMeleeEnemy::Attack(){
	AttackCapsule->SetGenerateOverlapEvents(true);
	Super::Attack();
}

void AMeleeEnemy::MakeDamage(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){
	Super::MakeDamage(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	AttackCapsule->SetGenerateOverlapEvents(false);
}
