// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"
#include "BaseWeapon.h"
#include "BasePickup.h"
#include "BombPickup.h"
#include "VitalityPickup.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

int32 UInventoryComponent::GetCurrentWeaponNum() const{
	return WeaponArray.Num();
}

int32 UInventoryComponent::GetCurrentBombNum() const{
	return BombArray.Num();
}

int32 UInventoryComponent::GetCurrentHealNum() const{
	return HealArray.Num();
}

int32 UInventoryComponent::GetWeaponMaxNum() const{
	return WeaponMaxNum;
}

int32 UInventoryComponent::GetBombMaxNum() const{
	return BombMaxNum;
}

int32 UInventoryComponent::GetHealMaxNum() const{
	return HealMaxNum;
}

