// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API UMyGameInstance : public UGameInstance{
	GENERATED_BODY()

public:
	// adds score points (args) to current score
	UFUNCTION(Category = "Score")
		void AddScore(int32 ScoreToadd);

	// clear current score points
	UFUNCTION(BlueprintCallable, Category = "Score")
		void FlushCurrentScore();

	// returns current player current score points
	UFUNCTION(BlueprintCallable, Category = "Score")
		int32 GetCurrentScore() const;
protected:
	// current player score points
	UPROPERTY()
		int32 CurrentScore;
};
