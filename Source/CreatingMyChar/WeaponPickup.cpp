// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponPickup.h"
#include "BaseWeapon.h"
#include "MyCharacter.h"


void AWeaponPickup::NotifyActorBeginOverlap(AActor* OtherActor){
	Super::NotifyActorBeginOverlap(OtherActor);
}

void AWeaponPickup::Pickup(){
	Super::Pickup();
	if (WeaponToPickupClass) {
		if (WeaponToPickupClass != PlayerCharacter->GetWeaponToSpawnClass()) {
			PlayerCharacter->SetWeaponToSpawnClass(WeaponToPickupClass);
			PlayerCharacter->DestroyCurrentWeapon();
			PlayerCharacter->SetCurrentWeapon();
			PlayerWeapon = PlayerCharacter->GetCurrentWeapon();
			if (PlayerWeapon){
				PlayerWeapon->OnReloadEnded.Broadcast();
				PlayerWeapon->OnAmmoPickedUp.Broadcast();
			}
		}
		else {
			PlayerWeapon = PlayerCharacter->GetCurrentWeapon();
			if (PlayerWeapon) {
				PlayerWeapon->PickupAmmo();
			}
		}
	}
	Destroy();
}
