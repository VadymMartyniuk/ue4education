// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MeleeEnemy.h"
#include "MasterGruntBossEnemy.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API AMasterGruntBossEnemy : public AMeleeEnemy{
	GENERATED_BODY()

public:
	AMasterGruntBossEnemy();

	void EnemyDied() override;

	void Attack() override;

	void MakeDamage(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		class UStaticMeshComponent* RightWeapon;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		class UStaticMeshComponent* LeftWeapon;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		FName LeftArmSocketName = "b_MF_Weapon_L";

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		FName RightArmSocketName = "b_MF_Weapon_R";

	UPROPERTY(VisibleAnywhere, Category = "Attack Capsule")
		class UCapsuleComponent* SecondAttackCapsule;
	
};
