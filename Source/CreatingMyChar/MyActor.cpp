// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"

// Sets default values
AMyActor::AMyActor() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay() {
	Super::BeginPlay();
	//CheckPerformance();
}

void AMyActor::MoveActor(float DeltaTime) {
	float DistanceToMove = Step * DeltaTime;
	if (bUpDirection) {
		AddActorWorldOffset(FVector(0, 0, DistanceToMove));
		CurrTime += DeltaTime;
		if (CurrTime >= TimeToFly) {
			bUpDirection = false;
		}
	}
	else {
		AddActorWorldOffset(FVector(0, 0, DistanceToMove * -1));
		CurrTime -= DeltaTime;
		if (CurrTime <= 0) {
			bUpDirection = true;
		}
	}
}

void AMyActor::CheckPerformance() {
	time_t StartTest = time(NULL);
	clock_t StartClock = clock();
	auto TestBegin = std::chrono::steady_clock::now();
	float SecStart = FPlatformTime::Seconds();

	FCollisionQueryParams Params;
	FHitResult Hit;

	for (size_t i = 0; i < 2000000; i++) {
		GetWorld()->LineTraceSingleByChannel(Hit, GetActorLocation(), GetActorLocation() + (GetActorLocation().GetSafeNormal() * 500), ECC_Visibility, Params);
	}

	time_t StopTest = time(NULL);
	clock_t StopClock = clock();
	auto TestEnd = std::chrono::steady_clock::now();
	float SecEnd = FPlatformTime::Seconds();

	float MsecEnd = SecEnd * 1000;
	float SecResult = difftime(StopTest, StartTest);
	float SecResultCtime = (float)(StopClock - StartClock) / CLOCKS_PER_SEC;
	float MsecResult = SecResultCtime * 1000;
	auto DiffSeconds = std::chrono::duration_cast<std::chrono::seconds>(TestEnd - TestBegin);
	auto DiffMSeconds = std::chrono::duration_cast<std::chrono::milliseconds>(TestEnd - TestBegin);
	float DateSec = SecEnd - SecStart;
	float DateMSec = DateSec * 1000;

	FString TimeString;
	TimeString.Append("\n seconds time.h: ");
	TimeString.Append(FString::SanitizeFloat(SecResult));
	TimeString.Append("\n seconds ctime: ");
	TimeString.Append(FString::SanitizeFloat(SecResultCtime));
	TimeString.Append("\n mseconds ctime: ");
	TimeString.Append(FString::SanitizeFloat(MsecResult));
	TimeString.Append("\n seconds chrono: ");
	TimeString.Append(FString::SanitizeFloat(DiffSeconds.count()));
	TimeString.Append("\n mseconds chrono: ");
	TimeString.Append(FString::SanitizeFloat(DiffMSeconds.count()));
	TimeString.Append("\n ue4time sec: ");
	TimeString.Append(FString::SanitizeFloat(DateSec));
	TimeString.Append("\n ue4time time msec: ");
	TimeString.Append(FString::SanitizeFloat(DateMSec));
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Black, *TimeString);
	}
}

// Called every frame
void AMyActor::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	AMyActor::MoveActor(DeltaTime);
}

