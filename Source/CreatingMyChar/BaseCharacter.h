// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UCharacterMovementComponent;

enum ECharacterMovementStates{
	IDLE,
	WALK,
	JOG,
	SPRINT,
	BOOSTED,
	JUMP,
	CROUCH,
	PRONE,
	SWIM,
	FLY
};

UCLASS(abstract)
class CREATINGMYCHAR_API ABaseCharacter : public ACharacter{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	//Moves character forward and back when input bindings active
	virtual void MoveForward(float ScaleValue);

	//Moves character right and left when input mappings active
	virtual void MoveRight(float ScaleValue);

	virtual void SetWalkSpeed(float SpeedValue);

	virtual void SetDefaultSpeed(float SpeedValue);

	virtual void SetSprintSpeed(float SpeedValue);

	virtual void SetBoostMultiply(float MultiplyValue);

	virtual void SetCrouchSpeed(float SpeedValue);

	virtual void SetProneSpeed(float SpeedValue);

	virtual void SetFlySpeed(float SpeedValue);

	virtual void SetSwimSpeed(float SpeedValue);

	virtual void SprintOn();
	virtual void SprintOff();

	virtual void WalkOn();
	virtual void WalkOff();

	virtual void StartFire();
	virtual void StopFire();

	//zoom in and zoom out character camera, when input mappings active changes TargetArmLength in USpringComponent for CameraStep value
	virtual void ZoomCameraIn();
	virtual void ZoomCameraOut();

protected:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		float MaxCameraLength;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		float MinCameraLength;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		float ZoomCameraMultiply;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		float WalkSpeed;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		float DefaultSpeed;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		float RunSpeed;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		float SwimSpeed;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		float FlySpeed;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		float CrouchSpeed;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		float ProneSpeed;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		UCameraComponent* PlayerCameraComponent;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		USpringArmComponent* SpringArmComponent;
};
