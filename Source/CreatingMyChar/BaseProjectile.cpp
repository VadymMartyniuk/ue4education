// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ABaseProjectile::ABaseProjectile(){
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	Bullet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet"));
	RootComponent = Bullet;
}

// Called when the game starts or when spawned
void ABaseProjectile::BeginPlay(){
	Super::BeginPlay();
	SetLifeSpan(SpawnLifeTime);
}

// Called every frame
void ABaseProjectile::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
}

void ABaseProjectile::NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit){
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved
		, HitLocation, HitNormal, NormalImpulse, Hit);
	if (OnHitFire.IsBound()) {
		OnHitFire.Execute(Hit);
	}
	Destroy();
}

