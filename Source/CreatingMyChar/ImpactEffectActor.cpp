// Fill out your copyright notice in the Description page of Project Settings.


#include "ImpactEffectActor.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "GameFramework//CharacterMovementComponent.h"
#include "Components/PrimitiveComponent.h"

// Sets default values
AImpactEffectActor::AImpactEffectActor(){
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AImpactEffectActor::BeginPlay(){
	Super::BeginPlay();
	SpawnEffects();
}

void AImpactEffectActor::SpawnEffects(){
	if (DecalMaterial) {
		FRotator DecalRotation = FRotationMatrix::MakeFromX(EffectHit.ImpactNormal).Rotator();
		float RandomRotation = FMath::FRandRange(-180, 180);
		DecalRotation.Roll = RandomRotation;
		UGameplayStatics::SpawnDecalAttached(DecalMaterial, DecalSize, EffectHit.GetComponent()
		, EffectHit.BoneName, EffectHit.ImpactPoint, DecalRotation, EAttachLocation::KeepWorldPosition, DecalLifeTime);	
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No decal material"));
	}
	if (EffectSound) {
		UGameplayStatics::PlaySoundAtLocation(this, EffectSound, EffectHit.ImpactPoint);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No impact effect sound"));
	}
	if (HitParticle) {
		UGameplayStatics::SpawnEmitterAtLocation(this, HitParticle, EffectHit.Location
			, EffectHit.ImpactPoint.Rotation(), HitParticleScale);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No hit particle"));
	}
	if (bApplyImpulse) {
		if (EffectHit.GetComponent()->Mobility == EComponentMobility::Movable) {
			if (Cast<ACharacter>(EffectHit.GetActor())) {
				Cast<ACharacter>(EffectHit.GetActor())->GetCharacterMovement()->AddImpulse
				((EffectHit.ImpactPoint - GetOwner()->GetOwner()->GetActorLocation().GetSafeNormal()) * ImpulseStrength, true);
			}
			else if (GetOwner()) {
				EffectHit.GetComponent()->AddImpulse((EffectHit.ImpactPoint - GetOwner()->GetOwner()->GetActorLocation().GetSafeNormal()) * ImpulseStrength);
			}
			else {
				UE_LOG(LogTemp, Warning, TEXT("Owner is empty AImpactEffectActor"));
			}
		}
	}
	if (EffectHit.GetActor()) {
		if (bIsRadialDamage) {
			UGameplayStatics::ApplyRadialDamage(this, Damage, EffectHit.ImpactNormal, RadialDamageRadius, nullptr, TArray<AActor*>(), GetOwner(), nullptr);
		}
		else {
			UGameplayStatics::ApplyDamage(EffectHit.GetActor(), Damage, nullptr, GetOwner(), nullptr);
		}
	}
}

float AImpactEffectActor::GetDecalLifeSpan() const{
	return DecalLifeTime;
}

// Called every frame
void AImpactEffectActor::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
}

void AImpactEffectActor::HitInit(FHitResult Hit){
	EffectHit = Hit;
}

