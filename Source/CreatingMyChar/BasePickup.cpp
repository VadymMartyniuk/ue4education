// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePickup.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "MyCharacter.h"
#include "Components/PointLightComponent.h"
#include "BaseEnemy.h"

// Sets default values
ABasePickup::ABasePickup(){
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	RootComponent = SceneComponent;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh Component"));
	StaticMeshComponent->SetGenerateOverlapEvents(false);
	StaticMeshComponent->AttachToComponent(RootComponent,FAttachmentTransformRules::KeepRelativeTransform);

	BoxCollidionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	BoxCollidionComponent->AttachToComponent(StaticMeshComponent, FAttachmentTransformRules::KeepRelativeTransform);
	BoxCollidionComponent->SetGenerateOverlapEvents(true);

	PointLightComponent = CreateDefaultSubobject<UPointLightComponent>(TEXT("Point Light Component"));
	PointLightComponent->AttachToComponent(BoxCollidionComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ABasePickup::BeginPlay(){
	Super::BeginPlay();
}

void ABasePickup::PlayPickupEffect(){
	if (PickupParticle){
		UGameplayStatics::SpawnEmitterAtLocation(this, PickupParticle, GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No pick up particle"));
	}
}

void ABasePickup::PlayPickupSound(){
	if (PickupSound) {
		UGameplayStatics::PlaySoundAtLocation(this, PickupSound, GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No pick up sound"));
	}
}

// Called every frame
void ABasePickup::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
}

void ABasePickup::NotifyActorBeginOverlap(AActor* OtherActor){
	Super::NotifyActorBeginOverlap(OtherActor);
	PlayerCharacter = Cast<AMyCharacter>(OtherActor);
	if (PlayerCharacter){
		if (StaticMeshComponent->IsVisible()) {
			Pickup();
			UE_LOG(LogTemp, Warning, TEXT("picked up"));
		}
	}
}

void ABasePickup::Pickup() {
	PlayPickupSound();
	PlayPickupEffect();
}

