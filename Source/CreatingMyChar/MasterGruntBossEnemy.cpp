// Fill out your copyright notice in the Description page of Project Settings.


#include "MasterGruntBossEnemy.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "BaseEnemy.h"
#include "MeleeEnemy.h"

AMasterGruntBossEnemy::AMasterGruntBossEnemy() {
	RightWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon"));
	RightWeapon->SetupAttachment(GetMesh(), RightArmSocketName);

	LeftWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon2"));
	LeftWeapon->SetupAttachment(GetMesh(), LeftArmSocketName);

	SecondAttackCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Attack Capsule Component2"));
	SecondAttackCapsule->SetupAttachment(LeftWeapon);
	SecondAttackCapsule->OnComponentBeginOverlap.AddDynamic(this, &AMasterGruntBossEnemy::MakeDamage);

	AttackCapsule->SetupAttachment(RightWeapon);
}

void AMasterGruntBossEnemy::EnemyDied() {
	Super::EnemyDied();

	RightWeapon->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	RightWeapon->SetEnableGravity(true);
	RightWeapon->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	RightWeapon->SetSimulatePhysics(true);
	RightWeapon = nullptr;

	LeftWeapon->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	LeftWeapon->SetEnableGravity(true);
	LeftWeapon->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	LeftWeapon->SetSimulatePhysics(true);
	LeftWeapon = nullptr;

	AttackCapsule->SetGenerateOverlapEvents(false);
	SecondAttackCapsule->SetGenerateOverlapEvents(false);
}

void AMasterGruntBossEnemy::Attack() {
	SecondAttackCapsule->SetGenerateOverlapEvents(true);
	Super::Attack();
}

void AMasterGruntBossEnemy::MakeDamage(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	Super::MakeDamage(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	SecondAttackCapsule->SetGenerateOverlapEvents(false);
}
