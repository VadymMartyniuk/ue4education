// Fill out your copyright notice in the Description page of Project Settings.
//Implementation of base class to all pick up
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasePickup.generated.h"

UCLASS()
class CREATINGMYCHAR_API ABasePickup : public AActor{
	GENERATED_BODY()
	
	public:	
	// Sets default values for this actor's properties
	ABasePickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//runs pick up particle effect
	void PlayPickupEffect();

	//plays pick up sound
	void PlayPickupSound();

	// base pick up logic
	virtual void Pickup();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//notify when actor overlaps with other actor and run pick up logic
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

protected:
	// pick up particle effect
	UPROPERTY(EditDefaultsOnly, Category = "Particles")
		UParticleSystem* PickupParticle;

	// pick up sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* PickupSound;

	// pick up static mesh 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Static Mesh")
		UStaticMeshComponent* StaticMeshComponent;

	//box collidion component 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Box Collidion")
		class UBoxComponent* BoxCollidionComponent;
	// player character class object
	UPROPERTY()
		class AMyCharacter* PlayerCharacter;

	// point light component to visualize point where on land pick up is
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PointLight")
		class UPointLightComponent* PointLightComponent;
};
