// Fill out your copyright notice in the Description page of Project Settings.


#include "VitalityPickup.h"
#include "MyCharacter.h"
#include "MyVitalityComponent.h"

void AVitalityPickup::NotifyActorBeginOverlap(AActor* OtherActor) {
	Super::NotifyActorBeginOverlap(OtherActor);
}

void AVitalityPickup::Pickup(){
	PlayerVitalityComponent = PlayerCharacter->GetVitalityComponent();
	if (PlayerVitalityComponent) {
		if (PlayerVitalityComponent->GetCurrentHealthPoint() < PlayerVitalityComponent->GetMaxHealthPoint()) {
			PlayerVitalityComponent->SetCurrentHealthPoint(HealthPointsToPickup);
			Super::Pickup();
			Destroy();
		}
	}	
}


