// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseEnemy.generated.h"

UCLASS()
class CREATINGMYCHAR_API ABaseEnemy : public ACharacter{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "Vitality")
		class UMyVitalityComponent* GetVitalityComponent() const;

	//caled to run death logic
	UFUNCTION()
		virtual void EnemyDied();

	UFUNCTION(BlueprintCallable)
		void SetDamageSize(float NewDamage);

	UFUNCTION()
		bool ShouldSpawnPickup(const int32 &Percent);

	UFUNCTION(BlueprintCallable)
		virtual void Attack();

	UFUNCTION(BlueprintCallable)
		virtual void StopAttack();

	UFUNCTION(BlueprintCallable)
		virtual void SpawnPickup();

	UFUNCTION(BlueprintCallable)
		virtual void MakeDamage(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// triggered when right foot touch floor
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void RightStepTrigger();

	// triggered when left foot touch floor
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void LeftStepTrigger();

	// spawns foot step smoke
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void SpawnStepSmoke(UArrowComponent* Foot, FName FootSocket);

	// plays foot step sound
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void PlayStepSound();

	UFUNCTION(BlueprintCallable, Category = "Montages")
		void PlayHitMontage();

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Player character")
		class AMyCharacter* PlayerCharacter;

	//component implements vitality logic
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Vitality")
		class UMyVitalityComponent* VitalityComponent;

	//skeletal mesh
	UPROPERTY(EditDefaultsOnly, Category = "Skeletal Mesh")
		USkeletalMesh* SkelMesh;

	// item to visualize healt points
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Widget")
		class UWidgetComponent* VitalityWidget;

	// AI component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
		class UAIPerceptionComponent* AIPerceptionComponent;

	// AI sense configuration
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
		class UAISenseConfig_Sight* SightConfig;

	// radius of enemy sight
	UPROPERTY(EditDefaultsOnly, Category = "AI")
		float EnemySightRadius = 500;

	// enemy lose sight radius
	UPROPERTY(EditDefaultsOnly, Category = "AI")
		float EnemyLoseSightRadius = 700;

	// enemy point of view
	UPROPERTY(EditDefaultsOnly, Category = "AI")
		float EnemyPOV = 70;

	UPROPERTY(EditDefaultsOnly, Category = "AI")
		float EnemyMaxAge = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
		float Damage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Life time")
		float LifeTime = 3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pick up")
		TArray<TSubclassOf<class ABasePickup> > PickupArray;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		TArray<class UAnimMontage*> AttackMontageArray;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		class UAnimMontage* TakeDamageMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		class USoundBase* AttackSound;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		class USoundBase* TakeDamageSound;

	// foot step sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* StepSound;

	// left foot socket name
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
		FName LeftFootSocketName;

	// right foot socket name
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
		FName RightFootSocketName;

	// arrow component to run right foot step logic (sount, particle)
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		class UArrowComponent* RightFoot;

	// arrow component to run left foot step logic (sount, particle)
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		UArrowComponent* LeftFoot;

	// step smoke particle effect
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Particles")
		UParticleSystem* StepSmoke;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Particles")
		UParticleSystem* DeathParticle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Boss")
		bool bIsBossEnemy = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Boss")
		float SpawnPickupAfterTime = 30;

	UPROPERTY()
		FTimerHandle PickupSpawnTimer;

	UPROPERTY(EditDefaultsOnly, Category = "Pickup")
		int32 PickupPossibilityPercent = 25;

	UPROPERTY()
		bool bIsSelfDestroy = false;
};
