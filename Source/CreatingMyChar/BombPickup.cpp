// Fill out your copyright notice in the Description page of Project Settings.


#include "BombPickup.h"
#include "BaseEnemy.h"
#include "Kismet/GameplayStatics.h"


void ABombPickup::NotifyActorBeginOverlap(AActor* OtherActor) {
	Super::NotifyActorBeginOverlap(OtherActor);

	Enemy = Cast<ABaseEnemy>(OtherActor);
	if (Enemy) {
		Explosion();
		UGameplayStatics::ApplyDamage(Enemy, DamageSize, nullptr, GetOwner(), nullptr);
		UE_LOG(LogTemp, Warning, TEXT("bomb explosion"));
	}
}

void ABombPickup::Explosion(){
	if (ExplosionParticle) {
		UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionParticle, GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No explosion particle"));
	}

	if (ExplosionSound) {
		UGameplayStatics::PlaySoundAtLocation(this, ExplosionSound, GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No explosion sound"));
	}

	Destroy();
}

void ABombPickup::Pickup(){
	Super::Pickup();
	Destroy();
}
