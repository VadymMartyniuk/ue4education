// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePickup.h"
#include "BombPickup.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API ABombPickup : public ABasePickup{
	GENERATED_BODY()

public:
	void NotifyActorBeginOverlap(AActor* OtherActor) override;

	void Explosion();

	void Pickup() override;

protected:
	// pick up particle effect
	UPROPERTY(EditDefaultsOnly, Category = "Particles")
		UParticleSystem* ExplosionParticle;

	// pick up sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* ExplosionSound;

	UPROPERTY()
		class ABaseEnemy* Enemy;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float DamageSize;



};
