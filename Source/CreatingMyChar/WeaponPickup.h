// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePickup.h"
#include "WeaponPickup.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API AWeaponPickup : public ABasePickup{
	GENERATED_BODY()

public:
	// notify when actor overlaps with other actor and run pick up logic
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	// weapon pick up logic
	virtual void Pickup() override;

protected:
	// weapon to pick up class object
	UPROPERTY(EditDefaultsOnly, Category = "WeaponToPickupClass")
		TSubclassOf<class ABaseWeapon> WeaponToPickupClass;

	// player character's current weapon
	UPROPERTY()
		ABaseWeapon* PlayerWeapon;
};
