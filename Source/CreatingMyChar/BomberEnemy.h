// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseEnemy.h"
#include "BomberEnemy.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API ABomberEnemy : public ABaseEnemy{
	GENERATED_BODY()

public:

	ABomberEnemy();

	void Attack() override;

	void MakeDamage(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Particles")
		UParticleSystem* ExplosionParticle;
};
