// Fill out your copyright notice in the Description page of Project Settings.
// base weapon class implementation
#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "BaseWeapon.generated.h"

class UAnimMontage;
class ACharacter;
class ABaseProjectile;
class UParticleSystem;
class UImpactEffectComponent;
// delegate to emit signals
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNotParamDelegate);

UCLASS()
class CREATINGMYCHAR_API ABaseWeapon : public AStaticMeshActor{
	GENERATED_BODY()

public:
	//sets default values
	ABaseWeapon();

public:
	//called when player pressed fire input, base weapon's fire start logic
	UFUNCTION()
		void StartFire();

	// base weapon's fire logic
	virtual void Fire();

	// base weapon's stop fire logic
	virtual void StopFire();

	//weapon reload 
	UFUNCTION()
		void Reload();
	//called when need to change fire montage in runtime
	UFUNCTION()
		void SetFireMontage(UAnimMontage* Montage);

	//runs equip animation
	UFUNCTION()
		void PlayEquipMontage();

	// called to change fire mode (single, auto)
	UFUNCTION()
		void SetFireMode();

	// checks if fire animation can be run. Runs fire animation. 
	UFUNCTION()
		bool bCanFire();

	// play no projectiles sound
	UFUNCTION()
		void PlayNoBulletsSound();

	// ammo pick up logic (can't pick up more projectiles then MaxAmmoSize value)
	UFUNCTION()
		void PickupAmmo();

protected:
	// called to set max projectiles count as current weapon projectiles count
	UFUNCTION()
		void SetCurrentBulletsCount();

	// plays reload animation
	UFUNCTION()
		void PlayReloadMontage();

	// plays weapon fire animation
	UFUNCTION()
		void PlayFireMontage();

	//plays weapon fire sound
	UFUNCTION()
		void PlayFireSound();

	// plays weapon reload sound
	UFUNCTION()
		void PlayReloadSound();

	// returns muzzle socket transform to spawn projectiles
	UFUNCTION()
		const FTransform GetMuzzleTransform();

	//called every frame
	virtual void Tick(float DeltaSeconds) override;

	//called when level started (event begin play) 
	virtual void BeginPlay() override;

	// spawns weapon fire particle effect
	UFUNCTION()
		void SpawnFireParticle();

public:
	// emits signal when current weapon projectiles count changed
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNotParamDelegate OnBulletsCountChanged;

	// emits signal when player changes weapon fire mode
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNotParamDelegate OnFireModeChanged;

	// emits signal when current weapon need to reload
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNotParamDelegate OnNeedToReload;

	// emits signal when reload animation ended
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNotParamDelegate OnReloadEnded;

	// emits signal when current weapon all projectiles ended
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNotParamDelegate OnAmmoEnded;

	// emits signal when pick up some projectiles
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNotParamDelegate OnAmmoPickedUp;

protected:
	// current weapon owner (pointer to player character object)
	UPROPERTY()
		class AMyCharacter* WeaponOwner;

	// impact effect component 
	UPROPERTY(VisibleAnywhere)
		UImpactEffectComponent* ImpactEffectComponent;

	// timer to run weapon's auto fire logic
	UPROPERTY()
		FTimerHandle FireTimer;

	// timer to run no projectiles logic when auto fire mode on
	UPROPERTY()
		FTimerHandle NoBulletsClickTimer;

	// muzzle socket name
	UPROPERTY(EditDefaultsOnly, Category = "Muzzle")
		FName MuzzleSocketName;

	// current magazine's projectiles count
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Ammo")
		int32 CurrentMagazineSize;

	// all projectiles of current weapon
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Ammo")
		int32 CurrentAmmoSize;

	// current weapon's maximum projectiles count 
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
		int32 MaxAmmoSize;

	// current weapon magazine's maximum projectiles count 
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
		int32 MaxMagazineSize;

	//  sets true if weapon in auto fire mode
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Fire mode")
		bool bIsAutoFire;

	// need to set false if weapon have one fire mode, true if more than one
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Fire mode")
		bool bIsCanChangeFireMode = true;

	// weapon fire speed multiplyer
	UPROPERTY(EditDefaultsOnly, Category = "Timers")
		float FireSpeedMultiplyer;

	// delay time before weapon fire
	UPROPERTY(EditDefaultsOnly, Category = "Timers")
		float FireDelay;

	// weapon click multiplyer when bullets count ended
	UPROPERTY(EditDefaultsOnly, Category = "Timers")
		float NoBulletsClickSpeedMultiplyer;

	// delay before click when bullets count ended
	UPROPERTY(EditDefaultsOnly, Category = "Timers")
		float NoBulletsClickDelay;
		
	// weapon fire animation
	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* FireMontage;

	// weapon reload animation
	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* ReloadMontage;

	// weapon fire sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* FireSound;

	// sound of delay before fire 
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* FireDelaySound;

	// reload sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* ReloadSound;

	// fire mode changing sound  
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* FireModeChangeSound;

	// bullets ended sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* NoBulletsSound;

	// weapon fire particle effect (muzzle)
	UPROPERTY(EditDefaultsOnly, Category = "Particles")
		UParticleSystem* FireParticle;

	// weapon fire particle system component
	UPROPERTY(BlueprintReadOnly, Category = "Particles")
		class UParticleSystemComponent* FireParticleComponent;

	/* fire particle effect scale 
	UPROPERTY(EditDefaultsOnly, Category = "Particles")
		FVector FireParticleScale;
	*/
	// weapon icon 
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Icon")
		class UTexture2D* WeaponIcon;

public:
	// weapon equip animation
	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* EquipMontage;

	// weapon equip animation length. delay before attaching weapon
	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		float TimeToAttachWeapon = 1;

protected:
		UPROPERTY(EditDefaultsOnly, Category = "Weapon")
			bool bEnemyCanFire = false;
};

