// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePickup.h"
#include "SpeedBoostPickup.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API ASpeedBoostPickup : public ABasePickup{
	GENERATED_BODY()
	
public:
	// notify when actor overlaps with other actor and run pick up logic
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	// speed boost pick up logic 
	virtual void Pickup() override;

protected:
	// stop speed boost logic
	UFUNCTION()
		void StopBoostedSpeed();

protected:
	// timer to stop speed boost logic
	UPROPERTY()
		FTimerHandle SpeedBoostTimer;

	// time of boost action
	UPROPERTY(EditDefaultsOnly, Category = "BoostTime")
		float BoostTime;
};
