// Fill out your copyright notice in the Description page of Project Settings.
// impact effect component class to run impact effect actor class logic

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ImpactEffectComponent.generated.h"

class AImpactEffectActor;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CREATINGMYCHAR_API UImpactEffectComponent : public UActorComponent{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UImpactEffectComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// called to spawn impact effect 
	UFUNCTION()
		void SpawnImpactEffect(FHitResult Hit);

protected:
	// impact effect actor class to spawn
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		TSubclassOf<AImpactEffectActor> ImpactActor_Class;

	UPROPERTY()
		AImpactEffectActor* ImpactActor;
};
