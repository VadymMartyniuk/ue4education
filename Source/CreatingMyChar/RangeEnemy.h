// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseEnemy.h"
#include "RangeEnemy.generated.h"

/**
 *
 */
UCLASS()
class CREATINGMYCHAR_API ARangeEnemy : public ABaseEnemy {
	GENERATED_BODY()

public:

	ARangeEnemy();

	void Attack() override;

	void StopAttack() override;

	void EnemyDied() override;

	void BeginPlay() override;

	void Tick(float DeltaTime) override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Muzzle")
		FName MuzzleSocketName;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* FireSound;

	// length of trace
	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float TraceLength;

	// beam particle effect
	UPROPERTY(EditDefaultsOnly, Category = "Beam")
		UParticleSystem* Beam;

	// beam particle effect component
	UPROPERTY(BlueprintReadOnly, Category = "Beam")
		class UParticleSystemComponent* BeamComponent;

	// beam particle effect scale factor
	UPROPERTY(EditDefaultsOnly, Category = "Beam")
		FVector BeamScale;

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float SphereRadius;

	UPROPERTY(BlueprintReadOnly, Category = "Attack")
		bool bIsAttacking = false;

	// impact effect component 
	UPROPERTY(VisibleAnywhere)
		class UImpactEffectComponent* ImpactEffectComponent;
};
