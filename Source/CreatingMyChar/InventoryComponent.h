// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CREATINGMYCHAR_API UInventoryComponent : public UActorComponent{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	int32 GetCurrentWeaponNum() const;

	int32 GetCurrentBombNum() const;

	int32 GetCurrentHealNum() const;

	int32 GetWeaponMaxNum() const;

	int32 GetBombMaxNum() const;

	int32 GetHealMaxNum() const;

protected:

	UPROPERTY()
		TArray<TSubclassOf<class ABaseWeapon>> WeaponArray;

	UPROPERTY()
		TArray<class ABombPickup*> BombArray;

	UPROPERTY()
		TArray<class AVitalityPickup*> HealArray;

	UPROPERTY()
		int32 WeaponMaxNum;

	UPROPERTY()
		int32 BombMaxNum;

	UPROPERTY()
		int32 HealMaxNum;


};
