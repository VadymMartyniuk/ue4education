// Fill out your copyright notice in the Description page of Project Settings.
//Implementation of enemy character base class 

#include "BaseEnemy.h"
#include "MyVitalityComponent.h"
#include "Widget.h"
#include "Components/WidgetComponent.h"
#include "UserWidget.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionComponent.h"
#include <Perception/AIPerceptionComponent.h>
#include <Perception/AISenseConfig_Sight.h>
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MyCharacter.h"
#include "BasePickup.h"
#include "Components/ArrowComponent.h"
#include "TimerManager.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ABaseEnemy::ABaseEnemy() {
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	if (SkelMesh) {
		GetMesh()->SetSkeletalMesh(SkelMesh);
		GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));
		GetMesh()->SetGenerateOverlapEvents(true);
	}

	VitalityComponent = CreateDefaultSubobject<UMyVitalityComponent>(TEXT("Vitality Component"));
	VitalityComponent->OnOwnerDied.AddDynamic(this, &ABaseEnemy::EnemyDied);
	VitalityWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("VitalityWidget"));
	VitalityWidget->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("AI Sight Configuration"));
	SightConfig->SightRadius = EnemySightRadius;
	SightConfig->LoseSightRadius = EnemyLoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = EnemyPOV;
	SightConfig->SetMaxAge(EnemyMaxAge);
	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;

	AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
	AIPerceptionComponent->ConfigureSense(*SightConfig);
	AIPerceptionComponent->SetDominantSense(SightConfig->GetSenseImplementation());

	RightFoot = CreateDefaultSubobject<UArrowComponent>(TEXT("Righ Foot"));
	LeftFoot = CreateDefaultSubobject<UArrowComponent>(TEXT("Left Foot"));
	RightFoot->SetupAttachment(GetMesh(), RightFootSocketName);
	LeftFoot->SetupAttachment(GetMesh(), LeftFootSocketName);
}

void ABaseEnemy::RightStepTrigger() {
	SpawnStepSmoke(RightFoot, RightFootSocketName);
	PlayStepSound();
}

void ABaseEnemy::LeftStepTrigger() {
	SpawnStepSmoke(LeftFoot, LeftFootSocketName);
	PlayStepSound();
}

void ABaseEnemy::SpawnStepSmoke(UArrowComponent* Foot, FName FootSocket) {
	if (StepSmoke) {
		FHitResult Hit;
		FVector StartTrace = Foot->GetSocketLocation(FootSocket) + FVector(0, 0, 20);
		FVector EndTrace = Foot->GetSocketLocation(FootSocket) - FVector(0, 0, 20);
		GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_Visibility);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), StepSmoke, Hit.Location, FRotator::ZeroRotator, FVector(0.2, 0.2, 0.2));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No enemy step smoke"));
	}
}

void ABaseEnemy::PlayStepSound() {
	if (StepSound) {
		UGameplayStatics::PlaySoundAtLocation(this, StepSound, GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No enemy step sound"));
	}
}

// Called when the game starts or when spawned
void ABaseEnemy::BeginPlay() {
	Super::BeginPlay();
	if (bIsBossEnemy) {
		GetWorld()->GetTimerManager().SetTimer(PickupSpawnTimer, this, &ABaseEnemy::SpawnPickup, SpawnPickupAfterTime, true, SpawnPickupAfterTime);
	}
}

// Called every frame
void ABaseEnemy::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ABaseEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

UMyVitalityComponent* ABaseEnemy::GetVitalityComponent() const{
	return VitalityComponent;
}

void ABaseEnemy::EnemyDied() {
	//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathParticle, GetActorLocation());
	GetMesh()->SetGenerateOverlapEvents(false);
	GetMesh()->SetEnableGravity(true);
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	VitalityWidget->Deactivate();

	SetLifeSpan(LifeTime);
	if (!bIsSelfDestroy) {
		SpawnPickup();
	}
	
	if (bIsBossEnemy) {
		GetWorld()->GetTimerManager().ClearTimer(PickupSpawnTimer);
	}

	UE_LOG(LogTemp, Warning, TEXT("Enemy died"));
}

void ABaseEnemy::SetDamageSize(float NewDamage) {
	Damage = NewDamage;
}

bool ABaseEnemy::ShouldSpawnPickup(const int32& Percent){
	return FMath::RandRange(1, 100 / Percent) == 1 ? true : false;
}

void ABaseEnemy::Attack() {
	if (AttackMontageArray.Num() > 0) {
		int32 RandIndex = FMath::RandRange(0, AttackMontageArray.Num() - 1);
		if (AttackMontageArray.IsValidIndex(RandIndex)) {
			PlayAnimMontage(AttackMontageArray[RandIndex]);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No enemy attack montage"));
	}
	if (AttackSound) {
		UGameplayStatics::PlaySoundAtLocation(this, AttackSound, GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No enemy attack sound"));
	}
}

void ABaseEnemy::StopAttack() {
	
}

void ABaseEnemy::PlayHitMontage() {
	if (TakeDamageMontage) {
		PlayAnimMontage(TakeDamageMontage);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No hit montage"));
	}
}

void ABaseEnemy::SpawnPickup() {
	if (ShouldSpawnPickup(PickupPossibilityPercent)) {
		if (PickupArray.Num() > 0) {
			int32 RandIndex = FMath::RandRange(0, PickupArray.Num() - 1);
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;

			if (GetWorld()) {
				if (PickupArray.IsValidIndex(RandIndex)) {
					GetWorld()->SpawnActor<ABasePickup>(PickupArray[RandIndex], GetActorLocation() + FVector(0, 50, 0), FRotator::ZeroRotator, SpawnParams);
					UE_LOG(LogTemp, Warning, TEXT("Enemy died, pick up spawned"));
				}
			}
		}
	}	
}

void ABaseEnemy::MakeDamage(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	PlayerCharacter = Cast<AMyCharacter>(OtherActor);
	if (PlayerCharacter) {
		UGameplayStatics::ApplyDamage(PlayerCharacter, Damage, GetController(), this, nullptr);
		UE_LOG(LogTemp, Warning, TEXT("player damaged"));
	}
}

