// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameInstance.h"

void UMyGameInstance::AddScore(int32 ScoreToadd){
	CurrentScore += ScoreToadd;
}

void UMyGameInstance::FlushCurrentScore(){
	CurrentScore = 0;
}

int32 UMyGameInstance::GetCurrentScore() const {
	return CurrentScore;
}
