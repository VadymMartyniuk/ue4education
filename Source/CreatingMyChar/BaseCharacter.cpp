// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"

// Sets default values
ABaseCharacter::ABaseCharacter(){
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay(){
	Super::BeginPlay();
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent){
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ABaseCharacter::MoveForward(float ScaleValue){

}

void ABaseCharacter::MoveRight(float ScaleValue){

}

void ABaseCharacter::SetWalkSpeed(float SpeedValue){

}

void ABaseCharacter::SetDefaultSpeed(float SpeedValue){

}

void ABaseCharacter::SetSprintSpeed(float SpeedValue){

}

void ABaseCharacter::SetBoostMultiply(float MultiplyValue){

}

void ABaseCharacter::SetCrouchSpeed(float SpeedValue){

}

void ABaseCharacter::SetProneSpeed(float SpeedValue){

}

void ABaseCharacter::SetFlySpeed(float SpeedValue){

}

void ABaseCharacter::SetSwimSpeed(float SpeedValue){

}

void ABaseCharacter::SprintOn(){

}

void ABaseCharacter::SprintOff(){

}

void ABaseCharacter::WalkOn(){

}

void ABaseCharacter::WalkOff(){

}

void ABaseCharacter::StartFire(){

}

void ABaseCharacter::StopFire(){

}

void ABaseCharacter::ZoomCameraIn(){

}

void ABaseCharacter::ZoomCameraOut(){

}

