// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseWeapon.h"
#include "RifleWeapon.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API ARifleWeapon : public ABaseWeapon{
	GENERATED_BODY()
	
public:
	UFUNCTION()
		void Fire() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Projectile")
		TSubclassOf<ABaseProjectile> ProjectileClass;
};
