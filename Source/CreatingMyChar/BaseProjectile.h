// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseProjectile.generated.h"

class UProjectileMovementComponent;
class UStaticMeshComponent;
//delegate to emit signal with FHitResult object in arguments
DECLARE_DYNAMIC_DELEGATE_OneParam(FHitResultDelegate, FHitResult, Hit);

UCLASS(abstract)
class CREATINGMYCHAR_API ABaseProjectile : public AActor{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// component to move projectile in world
	UPROPERTY(VisibleAnywhere)
		UProjectileMovementComponent* MovementComponent;

	// projectile static mesh
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* Bullet;

	// projectile life time in world
	UPROPERTY(EditDefaultsOnly, Category = "Bullet")
		float SpawnLifeTime;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//called when projectile hit something
	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

	// emit signal after projectile hit
	UPROPERTY()
		FHitResultDelegate OnHitFire;
};
