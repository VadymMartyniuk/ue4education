// Fill out your copyright notice in the Description page of Project Settings.


#include "RocketLauncherWeapon.h"
#include "BaseProjectile.h"
#include "ImpactEffectComponent.h"

void AGranadeLauncherWeapon::Fire(){
	if (CurrentMagazineSize > 0) {
		if (bCanFire()) {
			if (ProjectileClass) {
				FTransform TmpTransform = GetMuzzleTransform();
				ABaseProjectile* TmpProjectile = Cast<ABaseProjectile>(GetWorld()->SpawnActor(ProjectileClass, &TmpTransform));
				TmpProjectile->OnHitFire.BindDynamic(ImpactEffectComponent, &UImpactEffectComponent::SpawnImpactEffect);
				CurrentMagazineSize--;
				OnBulletsCountChanged.Broadcast();
			}
		}
	}
	else {
		Super::Fire();
	}
}
