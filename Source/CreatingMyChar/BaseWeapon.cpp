// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseWeapon.h"
#include "ImpactEffectComponent.h"
#include "MessageDialog.h"
#include "GameFramework/Character.h"
#include "BaseProjectile.h"
#include "ParticleHelper.h"
#include "Kismet/GameplayStatics.h"
#include "CollisionQueryParams.h"
#include "WorldCollision.h"
#include "Private/KismetTraceUtils.h"
#include "Components/StaticMeshComponent.h"
#include "MyCharacter.h"
#include "TimerManager.h"
#include "MyCharacter.h"


ABaseWeapon::ABaseWeapon(){
	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);

	ImpactEffectComponent = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("ImpactEffectComponent"));

	RootComponent = GetStaticMeshComponent();
}

void ABaseWeapon::StartFire(){
	if (CurrentMagazineSize > 0) {
		if (FireDelaySound) {
			UGameplayStatics::PlaySoundAtLocation(this,FireDelaySound,GetActorLocation());
		}
		if (bIsAutoFire) {
			GetWorld()->GetTimerManager().SetTimer(FireTimer, this, &ABaseWeapon::Fire, FireSpeedMultiplyer, bIsAutoFire, FireDelay);
		}
		else{
			Fire();
		}
	}
	else {
		StopFire();
		if (bIsAutoFire) {
			GetWorld()->GetTimerManager().SetTimer(NoBulletsClickTimer, this, &ABaseWeapon::PlayNoBulletsSound, NoBulletsClickSpeedMultiplyer, bIsAutoFire, NoBulletsClickDelay);
		}
		else{
			PlayNoBulletsSound();
		}
	}
}

void ABaseWeapon::StopFire(){
	if (CurrentAmmoSize > 0 && CurrentMagazineSize == 0) {
		OnNeedToReload.Broadcast();
	}
	else if (CurrentAmmoSize == 0 && CurrentMagazineSize == 0) {
		OnAmmoEnded.Broadcast();
	}
	GetWorld()->GetTimerManager().ClearTimer(FireTimer);
	GetWorld()->GetTimerManager().ClearTimer(NoBulletsClickTimer);
}

void ABaseWeapon::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);
}

void ABaseWeapon::Fire(){
	UE_LOG(LogTemp, Warning, TEXT("Trying to Fire"));
	if (bCanFire()) {
		GetWorld()->GetTimerManager().ClearTimer(FireTimer);
		GetWorld()->GetTimerManager().SetTimer(NoBulletsClickTimer, this, &ABaseWeapon::PlayNoBulletsSound, NoBulletsClickSpeedMultiplyer, bIsAutoFire, NoBulletsClickDelay);
	}
}

bool ABaseWeapon::bCanFire() {
	
	if (WeaponOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(EquipMontage)
		|| WeaponOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(ReloadMontage)) {
		return false;
	}
	else {
		PlayFireMontage();
		SpawnFireParticle();
		return true;
	}	
}

void ABaseWeapon::SpawnFireParticle() {
	if (FireParticle){
		UGameplayStatics::SpawnEmitterAttached(FireParticle, GetStaticMeshComponent(), MuzzleSocketName);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No fire particle"));
	}
}

const FTransform ABaseWeapon::GetMuzzleTransform() {
	if (MuzzleSocketName.IsNone()) {
		FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "MuzzleSocketName not define"));
		return FTransform::Identity;
	}
	return GetStaticMeshComponent()->GetSocketTransform(MuzzleSocketName);
}

void ABaseWeapon::SetCurrentBulletsCount(){
	CurrentMagazineSize = MaxMagazineSize;
	CurrentAmmoSize = MaxAmmoSize - MaxMagazineSize;
}

void ABaseWeapon::PickupAmmo() {
	if (CurrentAmmoSize + CurrentMagazineSize + MaxMagazineSize <= MaxAmmoSize) {
		CurrentAmmoSize += MaxMagazineSize;
	}
	else {
		CurrentAmmoSize = MaxAmmoSize - CurrentMagazineSize;
	}
	OnAmmoPickedUp.Broadcast();
}

void ABaseWeapon::Reload(){
	StopFire();
	if (((CurrentAmmoSize + CurrentMagazineSize) >= MaxMagazineSize) && CurrentMagazineSize < MaxMagazineSize) {
		PlayReloadMontage();
		CurrentAmmoSize += CurrentMagazineSize;
		CurrentMagazineSize = MaxMagazineSize;
		CurrentAmmoSize -= MaxMagazineSize;
	}
	else if (((CurrentAmmoSize + CurrentMagazineSize) < MaxMagazineSize) && CurrentMagazineSize < MaxMagazineSize && CurrentAmmoSize > 0){
		PlayReloadMontage();
		CurrentAmmoSize += CurrentMagazineSize;
		CurrentMagazineSize = CurrentAmmoSize;
		CurrentAmmoSize -= CurrentMagazineSize;
	}
	OnReloadEnded.Broadcast();
	OnBulletsCountChanged.Broadcast();
}

void ABaseWeapon::PlayReloadMontage(){
	if (ReloadMontage) {
		if (WeaponOwner) {
			if (!WeaponOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(EquipMontage)) {
				WeaponOwner->PlayAnimMontage(ReloadMontage);
				PlayReloadSound();
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found, Reload Montage"));
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No Reload Montage"));
	}
}

void ABaseWeapon::PlayFireMontage(){
	if (FireMontage) {
		if (WeaponOwner) {
			WeaponOwner->PlayAnimMontage(FireMontage);
			PlayFireSound();
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found, Fire Hip Montage"));
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No Fire Hip Montage"));
	}
}

void ABaseWeapon::PlayFireSound(){
	if (FireSound) {
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, WeaponOwner->GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No fire sound"));
	}
}

void ABaseWeapon::PlayNoBulletsSound(){
	if (NoBulletsSound) {
		UGameplayStatics::PlaySoundAtLocation(this, NoBulletsSound, WeaponOwner->GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No no bullets sound"));
	}
}

void ABaseWeapon::PlayReloadSound(){
	if (ReloadSound) {
		UGameplayStatics::PlaySoundAtLocation(this, ReloadSound, WeaponOwner->GetActorLocation());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No reload sound"));
	}
}

void ABaseWeapon::PlayEquipMontage(){
	if (EquipMontage) {
		if (WeaponOwner) {
			if (!WeaponOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(EquipMontage) 
				|| !WeaponOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(ReloadMontage)) {

				WeaponOwner->PlayAnimMontage(EquipMontage);
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found Equip Montage"));
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("No Equip Montage"));
	}
}

void ABaseWeapon::SetFireMode(){
	if (WeaponOwner && bIsCanChangeFireMode) {
		if (WeaponOwner->GetCurrentWeaponSocket() != WeaponOwner->GetWeaponBackSocket()) {
			if (bIsAutoFire) {
				bIsAutoFire = false;
				StopFire();
			}
			else {
				bIsAutoFire = true;
			}
			if (FireModeChangeSound) {
				UGameplayStatics::PlaySoundAtLocation(this, FireModeChangeSound, WeaponOwner->GetActorLocation());
			}
			OnFireModeChanged.Broadcast();
		}
	}
}

void ABaseWeapon::SetFireMontage(UAnimMontage* Montage){
	FireMontage = Montage;
}

void ABaseWeapon::BeginPlay(){
	Super::BeginPlay();
	SetCurrentBulletsCount();
	if (GetOwner()) {
		WeaponOwner = Cast<AMyCharacter>(GetOwner());
	}
	if(!WeaponOwner) {
		UE_LOG(LogTemp, Warning, TEXT("No Owner Found, Begin Play, Weapon"));
	}
}


