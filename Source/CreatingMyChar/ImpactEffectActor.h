// Fill out your copyright notice in the Description page of Project Settings.
// impact effect actor class implementation

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ImpactEffectActor.generated.h"

class UMaterialInterface;

UCLASS()
class CREATINGMYCHAR_API AImpactEffectActor : public AActor{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AImpactEffectActor();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// called to set effect hit params (FHitResult class object) 
	UFUNCTION()
		void HitInit(FHitResult Hit);
	// return decal life span
	UFUNCTION()
		float GetDecalLifeSpan() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// spawns impact effect actor and applies damage to hit actors 
	UFUNCTION()
		void SpawnEffects();

protected:
	// hit params
	UPROPERTY()
		FHitResult EffectHit;

	// impact effect's decal material 
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		UMaterialInterface* DecalMaterial;

	// impact effect's decal size
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		FVector DecalSize;

	// impact effect's decal life time 
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		float DecalLifeTime;

	// impact effect's sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* EffectSound;

	// impact effect's particle effect
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		UParticleSystem* HitParticle;

	// impact effect's hit particle scale 
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		FVector HitParticleScale;

	// sets if impact effect apply impulse after hit
	UPROPERTY(EditDefaultsOnly, Category = "Impulse")
		bool bApplyImpulse;

	// strength of impulse after hit
	UPROPERTY(EditDefaultsOnly, Category = "Impulse", meta = (EditCondition = "bApplyImpulse"))
		float ImpulseStrength;

	// damage applied after hit
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float Damage;

	// sets if impact effect applies radial damage
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		bool bIsRadialDamage = false;

	// radius of radial damage
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float RadialDamageRadius;
};
