// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RifleWeapon.h"
#include "RocketLauncherWeapon.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API AGranadeLauncherWeapon : public ABaseWeapon{
	GENERATED_BODY()

public:
	UFUNCTION()
		void Fire() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Projectile")
		TSubclassOf<ABaseProjectile> ProjectileClass;
};
