// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBoostPickup.h"
#include "GameFramework//CharacterMovementComponent.h"
#include "MyCharacter.h"
#include "TimerManager.h"
#include "Components/StaticMeshComponent.h"
#include "Components/PointLightComponent.h"


void ASpeedBoostPickup::NotifyActorBeginOverlap(AActor* OtherActor){
	Super::NotifyActorBeginOverlap(OtherActor);
}

void ASpeedBoostPickup::Pickup(){
	Super::Pickup();
	PlayerCharacter->SetBoostedSpeed();
	GetWorld()->GetTimerManager().SetTimer(SpeedBoostTimer, this, &ASpeedBoostPickup::StopBoostedSpeed, 1, false, BoostTime);
	StaticMeshComponent->SetVisibility(false);
	PointLightComponent->SetVisibility(false);
}

void ASpeedBoostPickup::StopBoostedSpeed() {
	GetWorld()->GetTimerManager().ClearTimer(SpeedBoostTimer);
	if (PlayerCharacter) {
		PlayerCharacter->SetDefaultSpeed();
	}
	Destroy();
}
