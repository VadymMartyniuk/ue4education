// Fill out your copyright notice in the Description page of Project Settinss implements player character logic
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNParamDelegate);

UCLASS()
class CREATINGMYCHAR_API AMyCharacter : public ACharacter{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//called every frame
	virtual void Tick(float DeltaTime) override;

	//Moves character forward and back when input bindings active
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void MoveForward(float ScaleValue);

	//Moves character right and left when input mappings active
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void MoveRight(float ScaleValue);

	// return player character current speed
	UFUNCTION(BlueprintCallable, Category = "Movement")
		float GetCurrentSpeed() const;

	// sets boosted speed (current speed * multiplyer)
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void SetBoostedSpeed();

	// sets current speed to default (jog)
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void SetDefaultSpeed();

	// sets current speed as sprint speed
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void SprintModeOn();

	// sets current speed as walk speed
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void WalkModeOn();

	// sets default speed
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void WalkModeOff();

	// rotates cplayer character to mouse cursor
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void RotateToMouse();

	// triggered when right foot touch floor
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void RightStepTrigger();

	// triggered when left foot touch floor
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void LeftStepTrigger();

	// spawns foot step smoke
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void SpawnStepSmoke(UArrowComponent* Foot, FName FootSocket);

	// plays foot step sound
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void PlayStepSound();

	// put weapon on back / on hand (run equip animation)
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void HideShowWeaponBack();

	// equip / unequip weapon logic (checks socket to attach, plays sound )
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void EquipWeapon();

	// weapon reload logic
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void WeaponReload();

	// weapon start fire 
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void StartFire();

	// weapon stop fire
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void StopFire();

	// creates base weapon class object and spawns on character's back socket
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		class ABaseWeapon* SpawnWeapon();

	// attachs current weapon to socket 
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void AttachWeapon(FName SocketName);

	// sets weapon fire mode (auto, single)
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void SetFireMode();

	// return current weapon class
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		TSubclassOf<ABaseWeapon> GetWeaponToSpawnClass() const;

	// sets weapon class
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void SetWeaponToSpawnClass(const TSubclassOf<ABaseWeapon> &WeaponClass);

	// sets current weapon spawns and attachs on character back socket
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void SetCurrentWeapon();

	// delete current weapon 
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void DestroyCurrentWeapon();

	// returns pointer to current weapon class object 
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		ABaseWeapon* GetCurrentWeapon();

	// returns socket name which weapon attached to
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		FName GetCurrentWeaponSocket() const;

	//  returns socket name which weapon attached to character back
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		FName GetWeaponBackSocket() const;

	// returns vitality component class object pointer
	UFUNCTION(BlueprintCallable, Category = "Vitality")
		class UMyVitalityComponent* GetVitalityComponent();

	// character died logic 
	UFUNCTION(BlueprintCallable, Category = "Vitality")
		void CharacterDied();

	//zoom in and zoom out character camera, when input mappings active changes TargetArmLength in USpringComponent for CameraStep value
	UFUNCTION(BlueprintCallable, Category = "Camera")
		void ZoomCameraIn();
	UFUNCTION(BlueprintCallable, Category = "Camera")
		void ZoomCameraOut();

protected:
	// on get weapon signal emiter
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNParamDelegate OnGetCurrentWeapon;

	// on weapon equip in hands signal emiter
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNParamDelegate OnWeaponEquip;

	// on weapon hide back signal emiter
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNParamDelegate OnWeaponUnEquip;

	// player camera component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		class UCameraComponent* PlayerCameraComponent;

	// camera line component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		class USpringArmComponent* SpringArmComponent;

	// default camera height 
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
		float DefaultCameraLength = 1000;

	// maximal camera height 
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
		float MaxCameraLength = 1200;

	// minimal camera height 
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
		float MinCameraLength = 800;

	// camera zoom step
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
		float ZoomCameraStep = 50;

	// walk speed value
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
		float WalkSpeed = 175;

	// default speed value (jog)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
		float DefaultSpeed = 350;

	// sprint speed value
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
		float SprintSpeed = 600;

	// speed boost multiplyer
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
		float SpeedBoostMultiplyer;

	// impulse to aplly when player character died
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Death")
		float DiedImpulse = 3000;

	// left foot socket name
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
		FName LeftFootSocketName;

	// right foot socket name
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
		FName RightFootSocketName;

	// arrow component to run right foot step logic (sount, particle)
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		class UArrowComponent* RightFoot;

	// arrow component to run left foot step logic (sount, particle)
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		UArrowComponent* LeftFoot;

	// weapon to spawn class 
	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		TSubclassOf<ABaseWeapon> WeaponToSpawnClass;

	// pointer to current weapon object
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "BaseWeapon")
		ABaseWeapon* CurrentWeapon;

	// arm socket name to attach weapon
	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName WeaponArmSocket;

	// back socket name to attach weapon
	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName WeaponBackSocket;
	
	// current socket name which weapon attached to
	UPROPERTY(BlueprintReadWrite, Category = "BaseWeapon")
		FName CurrentWeaponSocket;

	// player character skeletal mesh
	UPROPERTY(EditDefaultsOnly, Category = "Skeletal Mesh")
		USkeletalMesh* SkelMesh;

	// rifle fire from hip animation 
	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* FireRifleHipMontage;

	// rifle fire from sight animation
	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* FireRifleIronsightsMontage;

	// step smoke particle effect
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Particles")
		UParticleSystem* StepSmoke;

	// camera zoom in sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* CameraZoomInSound;

	// camera zoom out sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* CameraZoomOutSound;

	// foot step sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* StepSound;

	// weapon equip sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* EquipSound;

	// weapon unequip sound
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundBase* UnEquipSound;

	// player character vitality component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Vitality")
		class UMyVitalityComponent* VitalityComponent;
};
 