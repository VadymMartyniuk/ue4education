// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePickup.h"
#include "VitalityPickup.generated.h"

/**
 * 
 */
UCLASS()
class CREATINGMYCHAR_API AVitalityPickup : public ABasePickup{
	GENERATED_BODY()
	
public:
	// notify when actor overlaps with other actor and run pick up logic
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

protected:
	// vitality pick up logic
	virtual void Pickup() override;

	

protected:
	// picked up vitality points
	UPROPERTY(EditDefaultsOnly, Category = "HealthPoint")
		float HealthPointsToPickup;

	// vitality component class object 
	UPROPERTY(VisibleAnywhere, Category = "Vitality Component")
		class UMyVitalityComponent* PlayerVitalityComponent;
};
